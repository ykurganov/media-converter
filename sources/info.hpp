/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_INFO_HPP
#define MEDIA_CONVERTER_HEADER_INFO_HPP

#include <QStringList>

namespace MediaConverter
{
namespace Info
{
    const QString get_app_name();
    const QString get_app_internal_name();
    const QString get_app_version();
    const QString get_app_architecture();
    const QString get_app_dev_years();
    const QString get_app_domain();

    const QString get_author_name();
    const QString get_author_internal_name();
    const QString get_author_email();
    const QString get_author_domain();

    const QStringList get_media_wildcards();
    const QStringList get_media_filters();

    const QStringList get_program_filters();

    const QStringList get_profile_wildcards();
    const QStringList get_profile_filters();
}
}

#endif
