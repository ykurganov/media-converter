/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "service.hpp"
#include "logging.hpp"

#include <QMessageBox>

namespace MediaConverter
{
    Service::Service():
        Singleton<Service>(this)
    {
        std::set_terminate([](){
            MediaConverter::service()->fatal(QMessageBox::tr("Internal error"));
        });
    }

    void Service::information(const QString& message, QWidget* parent)
    {
        log(message);
        QMessageBox::information(parent, QMessageBox::tr("Information"), message, QMessageBox::Close);
    }

    void Service::warning(const QString& message, QWidget* parent)
    {
        log(message);
        QMessageBox::warning(parent, QMessageBox::tr("Warning"), message, QMessageBox::Close);
    }

    void Service::error(const QString& message, QWidget* parent)
    {
        log(message);
        QMessageBox::critical(parent, QMessageBox::tr("Error"), message, QMessageBox::Close);
    }

    void Service::critical(const QString& message, QWidget* parent)
    {
        log(message);
        QMessageBox::critical(parent, QMessageBox::tr("Critical error"), message, QMessageBox::Close);
    }

    void Service::fatal(const QString& message, QWidget* parent)
    {
        log(message);
        QMessageBox::critical(parent, QMessageBox::tr("Fatal error"), message, QMessageBox::Abort);
        std::abort();
    }
}
