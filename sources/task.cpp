/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "task.hpp"
#include "settingsmanager.hpp"
#include "settings.hpp"
#include "service.hpp"
#include "logging.hpp"

#include <QDir>

#include <functional>

extern "C"
{
#include <libavcodec/avcodec.h>
}

#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096

namespace MediaConverter
{

namespace
{

void decode(AVCodecContext *dec_ctx, AVPacket *pkt, AVFrame *frame, FILE *outfile)
{
    int i, ch;
    int ret, data_size;

    /* send the packet with the compressed data to the decoder */
    ret = avcodec_send_packet(dec_ctx, pkt);
    if (ret < 0) {
        service()->fatal("Error submitting the packet to the decoder");
    }

    /* read all the output frames (in general there may be any number of them */
    while (ret >= 0) {
        ret = avcodec_receive_frame(dec_ctx, frame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            return;
        else if (ret < 0) {
            service()->fatal("Error during decoding");
        }
        data_size = av_get_bytes_per_sample(dec_ctx->sample_fmt);
        if (data_size < 0) {
            service()->fatal("Failed to calculate data size");
        }
        for (i = 0; i < frame->nb_samples; i++)
            for (ch = 0; ch < dec_ctx->channels; ch++)
                fwrite(frame->data[ch] + data_size*i, 1, data_size, outfile);
    }
}

} // namespace

Task::Task(const QFileInfo& file_info, QObject* parent)
    : QThread(parent)
    , stop_request(false)
    , file_info(file_info)
{
    static int initialized;
    if (initialized)
        return;
    initialized = 1;
    avcodec_register_all();
}

void Task::stop()
{
    QMutexLocker mutex_locker(&mutex);
    stop_request = true;
}

void Task::run()
{
    const char *outfilename, *filename;
    const AVCodec *codec;
    AVCodecContext *c= NULL;
    AVCodecParserContext *parser = NULL;
    int len, ret;
    FILE *f, *outfile;
    uint8_t inbuf[AUDIO_INBUF_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];
    uint8_t *data;
    size_t   data_size;
    AVPacket *pkt;
    AVFrame *decoded_frame = NULL;

    auto filenameUtf8 = file_info.absoluteFilePath().toUtf8();
    filename    = filenameUtf8.constData();

    auto outfilenameUtf8 = (file_info.absolutePath() + QDir::separator() + file_info.completeBaseName() + ".wav").toUtf8();
    outfilename = outfilenameUtf8;

    /* find the MPEG audio decoder */
    codec = avcodec_find_decoder(AV_CODEC_ID_FLAC);
    if (!codec) {
        service()->fatal("Codec not found");
    }

    parser = av_parser_init(codec->id);
    if (!parser) {
        service()->fatal("Parser not found");
    }

    c = avcodec_alloc_context3(codec);
    if (!c) {
        service()->fatal("Could not allocate audio codec context");
    }

    /* open it */
    if (avcodec_open2(c, codec, NULL) < 0) {
        service()->fatal("Could not open codec");
    }

    f = fopen(filename, "rb");
    if (!f) {
        service()->fatal("Could not open file");
    }
    outfile = fopen(outfilename, "wb");
    if (!outfile) {
        av_free(c);
        service()->fatal("Could not open file");
    }

    /* decode until eof */
    data      = inbuf;
    data_size = fread(inbuf, 1, AUDIO_INBUF_SIZE, f);

    pkt = av_packet_alloc();

    while (data_size > 0) {
        if (!decoded_frame) {
            if (!(decoded_frame = av_frame_alloc())) {
                service()->fatal("Could not allocate audio frame");
            }
        }

        ret = av_parser_parse2(parser, c, &pkt->data, &pkt->size,
                               data, data_size,
                               AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
        if (ret < 0) {
            service()->fatal("Error while parsing");
        }
        data      += ret;
        data_size -= ret;

        if (pkt->size)
            decode(c, pkt, decoded_frame, outfile);

        if (data_size < AUDIO_REFILL_THRESH) {
            memmove(inbuf, data, data_size);
            data = inbuf;
            len = fread(data + data_size, 1,
                        AUDIO_INBUF_SIZE - data_size, f);
            if (len > 0)
                data_size += len;
        }
    }

    /* flush the decoder */
    pkt->data = NULL;
    pkt->size = 0;
    decode(c, pkt, decoded_frame, outfile);

    fclose(outfile);
    fclose(f);

    avcodec_free_context(&c);
    av_parser_close(parser);
    av_frame_free(&decoded_frame);
    av_packet_free(&pkt);
}

} // namespace MediaConverter
