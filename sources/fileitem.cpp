﻿/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "fileitem.hpp"
#include "logging.hpp"

#include <QDateTime>

namespace MediaConverter
{
    FileItem::FileItem(const QFileInfo& file_info):
        QTreeWidgetItem(QTreeWidgetItem::UserType),
        state(FileItemStateCreator::ready(this)),
        file_info(file_info)
    {
        setText(Column_Name, file_info.fileName());
        setToolTip(Column_Name, file_info.absoluteFilePath());

        setText(Column_Directory, file_info.absolutePath());
        setToolTip(Column_Directory, file_info.absolutePath());
    }

    void FileItem::translate()
    {
        state->translate(this);
    }

    void FileItem::queue()
    {
        if (is_ready()) {
            state = FileItemStateCreator::queued(this);
        }
    }

    void FileItem::start()
    {
        if (is_ready() || is_queued()) {
            task = QPointer<Task>(new Task(file_info, this));

            connect(task.data(), SIGNAL(finished()), task.data(), SLOT(deleteLater()));
            connect(task.data(), SIGNAL(finished()), this, SLOT(do_finished()));
            connect(task.data(), SIGNAL(finished()), this, SIGNAL(task_finished()));
            connect(task.data(), SIGNAL(completed()), this, SLOT(do_completed()));
            connect(task.data(), SIGNAL(progress_changed(int)), this, SLOT(do_progress_changed(int)));
            connect(task.data(), SIGNAL(statistics_changed(const QString&)), this, SLOT(do_statistics_changed(const QString&)));
            connect(task.data(), SIGNAL(error_occured()), this, SLOT(do_error_occured()));

            if (!info_dialog.isNull()) {
                connect(task.data(), SIGNAL(progress_changed(int)), info_dialog.data(), SLOT(setValue(int)));
                connect(task.data(), SIGNAL(statistics_changed(const QString&)), info_dialog.data(), SLOT(setLabelText(const QString&)));
            }

            state = FileItemStateCreator::running(this);
            task->start();
        }
    }

    void FileItem::stop()
    {
        if (is_running()) {
            task->stop();
            state = FileItemStateCreator::stopped(this);
        } else if (is_queued()) {
            state = FileItemStateCreator::ready(this);
        }
    }

    void FileItem::show_info_dialog()
    {
        if (info_dialog.isNull()) {
            info_dialog = new QProgressDialog(treeWidget());
            info_dialog->setWindowTitle(text(Column_Name));
            info_dialog->setLabelText(toolTip(Column_Status));
            info_dialog->setCancelButtonText(tr("Close"));
            info_dialog->setAutoClose(false);
            info_dialog->setAutoReset(false);
            info_dialog->setMinimum(0);
            info_dialog->setMaximum(100);
            info_dialog->setValue(data(Column_Status, Role_ProgressValue).toInt());
            info_dialog->setMinimumDuration(0);

            connect(this, SIGNAL(destroyed(QObject*)), info_dialog.data(), SLOT(cancel()));
            connect(info_dialog.data(), SIGNAL(canceled()), info_dialog.data(), SLOT(deleteLater()));

            if (!task.isNull()) {
                connect(task.data(), SIGNAL(progress_changed(int)), info_dialog.data(), SLOT(setValue(int)));
                connect(task.data(), SIGNAL(statistics_changed(const QString&)), info_dialog.data(), SLOT(setLabelText(const QString&)));
            }

            info_dialog->show();
        }

        info_dialog->raise();
        info_dialog->activateWindow();
    }

    void FileItem::do_finished()
    {
        state = FileItemStateCreator::finished(this);
    }

    void FileItem::do_completed()
    {
        state = FileItemStateCreator::completed(this);
    }

    void FileItem::do_progress_changed(int value)
    {
        setData(Column_Status, Role_ProgressValue, value);
    }

    void FileItem::do_statistics_changed(const QString& message)
    {
        setToolTip(Column_Status, message);
    }

    void FileItem::do_error_occured()
    {
        state = FileItemStateCreator::error(this);
    }
}
