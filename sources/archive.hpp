/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_ARCHIVE_HPP
#define MEDIA_CONVERTER_HEADER_ARCHIVE_HPP

#include <QList>
#include <QString>
#include <QSharedPointer>

namespace MediaConverter
{
    struct ArchiveEntry
    {
        QString path;
        QByteArray data;
    };

    typedef QSharedPointer<ArchiveEntry> ArchiveEntryPtr;
    typedef QList<ArchiveEntryPtr> ArchiveEntries;

    const ArchiveEntries extract_all(const QString& path);
    const ArchiveEntryPtr find(const ArchiveEntries&, const QString& path);
}

#endif
