/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_FILE_ITEM_STATE_HPP
#define MEDIA_CONVERTER_HEADER_FILE_ITEM_STATE_HPP

#include <QSharedPointer>

namespace MediaConverter
{
    // -------------------------BEGIN-------------------------

    class FileItemState
    {
    public:
        virtual ~FileItemState() {}
        virtual void translate(class FileItem*) const = 0;
        virtual bool is_ready() const { return false; }
        virtual bool is_queued() const { return false; }
        virtual bool is_running() const { return false; }
    };

    // -------------------------READY-------------------------

    class FileItemStateReady: public FileItemState
    {
    public:
        explicit FileItemStateReady(class FileItem*);
        virtual void translate(class FileItem*) const override;
        virtual bool is_ready() const override { return true; }
    };

    // -------------------------QUEUED-------------------------

    class FileItemStateQueued: public FileItemState
    {
    public:
        explicit FileItemStateQueued(class FileItem*);
        virtual void translate(class FileItem*) const override;
        virtual bool is_queued() const override { return true; }
    };

    // -------------------------RUNNING-------------------------

    class FileItemStateRunning: public FileItemState
    {
    public:
        explicit FileItemStateRunning(class FileItem*);
        virtual void translate(class FileItem*) const override;
        virtual bool is_running() const override { return true; }
    };

    // -------------------------FINISHED-------------------------

    class FileItemStateFinished: public FileItemState
    {
    public:
        explicit FileItemStateFinished(class FileItem*);
        virtual void translate(class FileItem*) const override;
        virtual bool is_ready() const override { return true; }
    };

    // -------------------------STOPPED-------------------------

    class FileItemStateStopped: public FileItemState
    {
    public:
        explicit FileItemStateStopped(class FileItem*);
        virtual void translate(class FileItem*) const override;
        virtual bool is_ready() const override { return true; }
    };

    // -------------------------COMPLETED-------------------------

    class FileItemStateCompleted: public FileItemState
    {
    public:
        explicit FileItemStateCompleted(class FileItem*);
        virtual void translate(class FileItem*) const override;
        virtual bool is_ready() const override { return true; }
    };

    // -------------------------ERROR-------------------------

    class FileItemStateError: public FileItemState
    {
    public:
        explicit FileItemStateError(class FileItem*);
        virtual void translate(class FileItem*) const override;
        virtual bool is_ready() const override { return true; }
    };

    // -------------------------END-------------------------

    typedef QSharedPointer<FileItemState> FileItemStatePtr;

    class FileItemStateCreator
    {
    public:
        static FileItemStatePtr ready(class FileItem* file_item) { return QSharedPointer<FileItemStateReady>::create(file_item); }
        static FileItemStatePtr queued(class FileItem* file_item) { return QSharedPointer<FileItemStateQueued>::create(file_item); }
        static FileItemStatePtr running(class FileItem* file_item) { return QSharedPointer<FileItemStateRunning>::create(file_item); }
        static FileItemStatePtr finished(class FileItem* file_item) { return QSharedPointer<FileItemStateFinished>::create(file_item); }
        static FileItemStatePtr stopped(class FileItem* file_item) { return QSharedPointer<FileItemStateStopped>::create(file_item); }
        static FileItemStatePtr completed(class FileItem* file_item) { return QSharedPointer<FileItemStateCompleted>::create(file_item); }
        static FileItemStatePtr error(class FileItem* file_item) { return QSharedPointer<FileItemStateError>::create(file_item); }
    };
}

#endif
