/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_SINGLETON_HPP
#define MEDIA_CONVERTER_HEADER_SINGLETON_HPP

#include <QAtomicPointer>

namespace MediaConverter
{
    template <typename T>
    class Singleton
    {
        Q_DISABLE_COPY(Singleton)

    protected:
        explicit Singleton(T* instance);
        ~Singleton();

    public:
        static T* get_instance();

    private:
        static QAtomicPointer<T> instance;
    };

    template <typename T>
    Singleton<T>::Singleton(T* instance)
    {
        bool ok = this->instance.testAndSetRelaxed(0, instance);
        Q_ASSERT_X(ok, Q_FUNC_INFO, "Only one instance of this class is permitted");
    }

    template <typename T>
    Singleton<T>::~Singleton()
    {
        T* old_instance = instance.fetchAndStoreRelaxed(0);
        Q_ASSERT_X(0 != old_instance, Q_FUNC_INFO, "The singleton instance is invalid");
    }

    template <typename T>
    T* Singleton<T>::get_instance()
    {
        Q_ASSERT_X(0 != instance, Q_FUNC_INFO, "The singleton has not yet been created");
        return instance;
    }

    template <typename T>
    QAtomicPointer<T> Singleton<T>::instance;
}

#endif
