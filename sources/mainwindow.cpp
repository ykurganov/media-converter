/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.hpp"
#include "info.hpp"
#include "logging.hpp"
#include "service.hpp"
#include "translation.hpp"
#include "configuredialog.hpp"
#include "settingsmanager.hpp"
#include "settings.hpp"

#include "ui_mainwindow.h"
#include "ui_historydialog.h"

#include <QSignalMapper>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>

namespace MediaConverter
{
    MainWindow::MainWindow():
        ui(new Ui::MainWindow),
        language_action_group(new QActionGroup(this))
    {
        ui->setupUi(this);

        connect(Translation::get_instance(), SIGNAL(language_changed()), ui->translate_action, SLOT(trigger()));

        QActionGroup* tool_button_style_action_group = new QActionGroup(this);
        tool_button_style_action_group->addAction(ui->icon_only_action);
        tool_button_style_action_group->addAction(ui->text_only_action);
        tool_button_style_action_group->addAction(ui->text_beside_icon_action);
        tool_button_style_action_group->addAction(ui->text_under_icon_action);

        QSignalMapper* tool_button_style_signal_mapper = new QSignalMapper(this);
        tool_button_style_signal_mapper->setMapping(ui->icon_only_action, Qt::ToolButtonIconOnly);
        tool_button_style_signal_mapper->setMapping(ui->text_only_action, Qt::ToolButtonTextOnly);
        tool_button_style_signal_mapper->setMapping(ui->text_beside_icon_action, Qt::ToolButtonTextBesideIcon);
        tool_button_style_signal_mapper->setMapping(ui->text_under_icon_action, Qt::ToolButtonTextUnderIcon);

        connect(tool_button_style_signal_mapper, SIGNAL(mapped(int)), this, SLOT(change_tool_button_style(int)));

        connect(ui->icon_only_action, SIGNAL(triggered()), tool_button_style_signal_mapper, SLOT(map()));
        connect(ui->text_only_action, SIGNAL(triggered()), tool_button_style_signal_mapper, SLOT(map()));
        connect(ui->text_beside_icon_action, SIGNAL(triggered()), tool_button_style_signal_mapper, SLOT(map()));
        connect(ui->text_under_icon_action, SIGNAL(triggered()), tool_button_style_signal_mapper, SLOT(map()));

        connect(ui->about_qt_action, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

        QAction* default_language_action = new QAction(language_action_group);
        default_language_action->setObjectName(Translation::get_instance()->get_default_language_name());
        default_language_action->setText(Translation::get_instance()->get_default_language_name());
        default_language_action->setIcon(Translation::get_instance()->get_default_language_flag());
        default_language_action->setCheckable(true);
        connect(default_language_action, SIGNAL(triggered()), this, SLOT(change_language()));
        ui->language_menu->addAction(default_language_action);

        for (const QString& language_name: Translation::get_instance()->get_additional_language_names())
        {
            QAction* language_action = new QAction(language_action_group);
            language_action->setObjectName(language_name);
            language_action->setText(language_name);
            language_action->setIcon(Translation::get_instance()->get_additional_language_flag(language_name));
            language_action->setCheckable(true);
            connect(language_action, SIGNAL(triggered()), this, SLOT(change_language()));
            ui->language_menu->addAction(language_action);
        }

        if (QAction* language_action = findChild<QAction*>(Translation::get_instance()->get_current_language_name())) {
            language_action->setChecked(true);
        } else {
            log(tr("Can't find language '%1'").arg(Translation::get_instance()->get_current_language_name()));
        }

        translate();

        restoreGeometry(settings_manager()->get_settings()->MainWindow.Geometry());
        restoreState(settings_manager()->get_settings()->MainWindow.State());
        ui->file_item_list->header()->restoreState(settings_manager()->get_settings()->FileList.Header.State());
    }

    MainWindow::~MainWindow()
    {
        settings_manager()->get_settings()->FileList.Header.SetState(ui->file_item_list->header()->saveState());
        settings_manager()->get_settings()->MainWindow.SetState(saveState());
        settings_manager()->get_settings()->MainWindow.SetGeometry(saveGeometry());
    }

    void MainWindow::translate()
    {
        ui->retranslateUi(this);
        setWindowTitle(Info::get_app_name());
        for (QAction* language_action: language_action_group->actions()) {
            language_action->setStatusTip(tr("Change language to '%1'").arg(language_action->text()));
        }
    }

    void MainWindow::show_history()
    {
        QDialog dialog(this);
        Ui::HistoryDialog ui;
        ui.setupUi(&dialog);
        ui.text_browser->setPlainText(logger()->get_messages().join('\n'));
        dialog.exec();
    }

    void MainWindow::change_tool_button_style(int style)
    {
        Q_ASSERT(Qt::ToolButtonIconOnly <= style && style <= Qt::ToolButtonTextUnderIcon);
        setToolButtonStyle(static_cast<Qt::ToolButtonStyle>(style));
    }

    void MainWindow::change_language()
    {
        if (QAction* language_action = qobject_cast<QAction*>(sender())) {
            if (!Translation::get_instance()->install_language(language_action->objectName())) {
                service()->error(tr("Can't install language '%1'").arg(language_action->objectName()), this);
            }
        } else {
            Q_ASSERT_X(false, Q_FUNC_INFO, "The structure of connections is damaged");
        }
    }

    void MainWindow::add_files()
    {
        const QStringList files = QFileDialog::getOpenFileNames(this, tr("Add files"), settings_manager()->get_settings()->LastOpenFilePath(), Info::get_media_filters().join(";;"));
        if (!files.isEmpty()) {
            settings_manager()->get_settings()->SetLastOpenFilePath(QFileInfo(files.first()).absolutePath());
            ui->file_item_list->add_entries(files);
        }
    }

    void MainWindow::add_directory()
    {
        const QString directory = QFileDialog::getExistingDirectory(this, tr("Add directory"), settings_manager()->get_settings()->LastOpenDirectoryPath());
        if (!directory.isEmpty()) {
            settings_manager()->get_settings()->SetLastOpenDirectoryPath(QFileInfo(directory).absoluteFilePath());
            ui->file_item_list->add_entries(QStringList(directory));
        }
    }

    void MainWindow::configure()
    {
        ConfigureDialog dialog(this);
        dialog.exec();
    }

    void MainWindow::about()
    {
        QMessageBox::information(
            this,
            tr("About"),
            tr("%1, version %2, architecture %3"
               "<br>Copyright (C) %4 %5 &lt;<a href=mailto:%6?subject&#61;%7>%6</a>&gt;"
               "<br><a href=%8>%8</a>"
              ).arg(Info::get_app_name())
               .arg(Info::get_app_version())
               .arg(Info::get_app_architecture())
               .arg(Info::get_app_dev_years())
               .arg(Info::get_author_name())
               .arg(Info::get_author_email())
               .arg(Info::get_app_internal_name())
               .arg(Info::get_app_domain())
        );
    }

    void MainWindow::closeEvent(QCloseEvent* event)
    {
        if (ui->file_item_list->has_running_items() && QMessageBox::No == QMessageBox::question(this, Info::get_app_name(), tr("There are unfinished conversions.<br>Are you sure you want to exit?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No)) {
            event->ignore();
        }
    }
}
