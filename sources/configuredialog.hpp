/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_CONFIGURE_DIALOG_HPP
#define MEDIA_CONVERTER_HEADER_CONFIGURE_DIALOG_HPP

#include <QDialog>
#include <QSharedPointer>

namespace Ui
{
    class ConfigureDialog;
}

namespace MediaConverter
{
    class ConfigureDialog: public QDialog
    {
        Q_OBJECT

    public:
        explicit ConfigureDialog(QWidget* parent = nullptr);

    private Q_SLOTS:
        void validate();

        void autodetect_simultaneous_conversions();

        void load_profile();
        void save_profile();

        void reset();
        void apply();

    private:
        void load(const QSharedPointer<class Settings>&);
        void save(const QSharedPointer<class Settings>&) const;

    private:
        QSharedPointer<Ui::ConfigureDialog> ui;
    };
}

#endif
