/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_FILE_WRAPPER_HPP
#define MEDIA_CONVERTER_HEADER_FILE_WRAPPER_HPP

#include <QFile>
#include <QByteArray>

namespace MediaConverter
{
    class FileWrapper
    {
    public:
        FileWrapper(const QString& path, QFile::OpenMode mode);

        const QString path() const { return file.fileName(); }

        qint64 size() const { return file.size(); }
        qint64 pos() const { return file.pos(); }

        bool at_end() const { return file.atEnd(); }

        qint64 read(char* data, qint64 max_size);
        const QByteArray read() { return file.readAll(); }

        qint64 write(const char* data, qint64 max_size);
        void write(const QByteArray& data) { write(data.data(), data.size()); }

    private:
        QFile file;
    };

    const QByteArray read(FileWrapper& file, qint64 max_size);
}

#endif
