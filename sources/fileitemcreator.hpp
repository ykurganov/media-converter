/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_FILE_ITEM_CREATOR_HPP
#define MEDIA_CONVERTER_HEADER_FILE_ITEM_CREATOR_HPP

#include "fileitem.hpp"

#include <QDir>
#include <QTimer>
#include <QThread>
#include <QMutex>
#include <QStringList>
#include <QTreeWidgetItem>

namespace MediaConverter
{
    class FileItemCreator: public QThread
    {
        Q_OBJECT

    public:
        FileItemCreator(const QStringList& entries, QThread* target_thread, bool recursively);

        bool is_canceled() const;

    public Q_SLOTS:
        void cancel();

    Q_SIGNALS:
        void about_to_finish();
        void canceled();
        void completed(const QList<QTreeWidgetItem*>& items);
        void progress_changed(int value);
        void progress_changed(const QString& message);

    protected:
        virtual void run();

    private Q_SLOTS:
        void emit_progress_changed();

    private:
        void add_entries(const QFileInfoList& entry_infos);
        void add_dirs(const QFileInfoList& dir_infos);
        void add_files(const QFileInfoList& file_infos);

    private:
        QStringList entries;
        QThread* target_thread;
        QDir::Filters filters;
        QStringList wildcards;
        QList<FileItem*> items;
        QTimer statistics_timer;
        QString current_dir;
        bool cancel_request;
        mutable QMutex mutex;
    };
}

#endif
