/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "filewrapper.hpp"
#include "exception.hpp"

#include <QVector>

namespace
{
    const QString to_string(QFile::FileError error)
    {
        switch (error) {
        case QFile::NoError:
            return QFile::tr("No error occurred");
        case QFile::ReadError:
            return QFile::tr("An error occurred when reading from the file");
        case QFile::WriteError:
            return QFile::tr("An error occurred when writing to the file");
        case QFile::FatalError:
            return QFile::tr("A fatal error occurred");
        case QFile::ResourceError:
            return QFile::tr("A resource error occurred");
        case QFile::OpenError:
            return QFile::tr("The file could not be opened");
        case QFile::AbortError:
            return QFile::tr("The operation was aborted");
        case QFile::TimeOutError:
            return QFile::tr("A timeout occurred");
        case QFile::UnspecifiedError:
            return QFile::tr("An unspecified error occurred");
        case QFile::RemoveError:
            return QFile::tr("The file could not be removed");
        case QFile::RenameError:
            return QFile::tr("The file could not be renamed");
        case QFile::PositionError:
            return QFile::tr("The position in the file could not be changed");
        case QFile::ResizeError:
            return QFile::tr("The file could not be resized");
        case QFile::PermissionsError:
            return QFile::tr("The file could not be accessed");
        case QFile::CopyError:
            return QFile::tr("The file could not be copied");
        default:
            Q_ASSERT_X(false, Q_FUNC_INFO, "Unsupported Qt version");
            return QFile::tr("Unsupported Qt version");
        }
    }
}

namespace MediaConverter
{
    FileWrapper::FileWrapper(const QString& path, QFile::OpenMode mode):
        file(path)
    {
        if (!file.open(mode)) {
            throw Exception(QFile::tr("Can't open file '%1' (%2: %3)").arg(path, to_string(file.error()), file.errorString()));
        }
    }

    qint64 FileWrapper::read(char* data, qint64 max_size)
    {
        const qint64 read_count = file.read(data, max_size);
        if (-1 == read_count) {
            throw Exception(QFile::tr("Can't read file '%1' (%2: %3)").arg(path(), to_string(file.error()), file.errorString()));
        }
        return read_count;
    }

    qint64 FileWrapper::write(const char* data, qint64 max_size)
    {
        const qint64 write_count = file.write(data, max_size);
        if (-1 == write_count) {
            throw Exception(QFile::tr("Can't write file '%1' (%2: %3)").arg(path(), to_string(file.error()), file.errorString()));
        }
        return write_count;
    }

    const QByteArray read(FileWrapper& file, qint64 max_size)
    {
        QVector<char> buffer(max_size);
        const qint64 real_size = file.read(buffer.data(), max_size);
        return QByteArray(buffer.data(), real_size);
    }
}
