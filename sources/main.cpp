/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "logging.hpp"
#include "service.hpp"
#include "translation.hpp"
#include "mainwindow.hpp"
#include "settingsmanager.hpp"
#include "settings.hpp"

#include <QApplication>

int main(int argc, char* argv[])
{
    QApplication application(argc, argv);

    Q_INIT_RESOURCE(resource);

    MediaConverter::Logger logger;
    MediaConverter::Service service;
    MediaConverter::SettingsManager settings_manager;
    MediaConverter::Translation translation;
    MediaConverter::MainWindow main_window;

    main_window.show();
    return application.exec();
}
