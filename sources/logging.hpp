/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_LOGGING_HPP
#define MEDIA_CONVERTER_HEADER_LOGGING_HPP

#include "singleton.hpp"

#include <QStringList>

namespace MediaConverter
{
    class Logger: public Singleton<Logger>
    {
    public:
        Logger();

        void log(const QString&);

        const QStringList& get_messages() const { return messages; }

    private:
        QStringList messages;
    };

    inline Logger* logger()
    {
        return Logger::get_instance();
    }

    inline void log(const QString& message)
    {
        logger()->log(message);
    }
}

#endif
