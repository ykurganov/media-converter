/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "fileitemdelegate.hpp"
#include "fileitem.hpp"

#include <QApplication>
#include <QPainter>

namespace MediaConverter
{
    FileItemDelegate::FileItemDelegate(QObject* parent):
        QStyledItemDelegate(parent)
    {
        progress_bar.setMinimum(0);
        progress_bar.setMaximum(100);
        progress_bar.setTextVisible(true);
        progress_bar.setAlignment(Qt::AlignCenter);
    }

    void FileItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
    {
        if (index.column() == FileItem::Column_Status) {
            progress_bar.setValue(index.data(FileItem::Role_ProgressValue).toInt());
            progress_bar.setFormat(index.data(FileItem::Role_ProgressFormat).toString());
            progress_bar.setStyleSheet(index.data(FileItem::Role_ProgressStyleSheet).toString());

            painter->save();
            painter->translate(option.rect.topLeft());
            progress_bar.resize(option.rect.size());
            progress_bar.render(painter);
            painter->restore();
        } else {
            QStyledItemDelegate::paint(painter, option, index);
        }
    }
}
