/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "fileitemlist.hpp"
#include "fileitemdelegate.hpp"
#include "fileitemcreator.hpp"
#include "info.hpp"
#include "logging.hpp"
#include "settingsmanager.hpp"
#include "settings.hpp"

#include <QApplication>
#include <QMimeData>
#include <QMessageBox>
#include <QHeaderView>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDropEvent>

using namespace MediaConverter;

namespace
{
    const int q_tree_widget_item_ptr_list_id = qRegisterMetaType<QList<QTreeWidgetItem*>>("QList<QTreeWidgetItem*>");

    struct FileItemExists: std::binary_function<const FileItem*, QList<FileItem*>, bool>
    {
        bool operator()(const FileItem* item, const QList<FileItem*>& items) const
        {
            return items.constEnd() != std::find_if(items.constBegin(), items.constEnd(), std::bind2nd(std::mem_fun(&FileItem::is_equivalent), item));
        }
    };

    struct FileItemCaster: std::unary_function<QTreeWidgetItem*, FileItem*>
    {
        FileItem* operator()(QTreeWidgetItem* item) const
        {
            return static_cast<FileItem*>(item);
        }
    };

    const QList<QTreeWidgetItem*> all_items(QTreeWidget* tree_widget)
    {
        QTreeWidgetItemIterator iter(tree_widget);
        QList<QTreeWidgetItem*> items;
        while (*iter) {
            items.append(*iter++);
        }
        return items;
    }

    FileItem* qitem_cast(QTreeWidgetItem* item)
    {
        return FileItemCaster()(item);
    }

    const QList<FileItem*> qitems_cast(const QList<QTreeWidgetItem*>& qitems)
    {
        QList<FileItem*> items;
        std::transform(qitems.constBegin(), qitems.constEnd(), std::back_inserter(items), FileItemCaster());
        return items;
    }

    const QList<QTreeWidgetItem*> items_cast(const QList<FileItem*>& items)
    {
        QList<QTreeWidgetItem*> qitems;
        std::copy(items.constBegin(), items.constEnd(), std::back_inserter(qitems));
        return qitems;
    }
}

namespace MediaConverter
{
    FileItemList::FileItemList(QWidget* parent):
        QTreeWidget(parent)
    {
        setItemDelegate(new FileItemDelegate(this));

        connect(model(), SIGNAL(rowsInserted(const QModelIndex&, int, int)), this, SLOT(check_emptiness()));
        connect(model(), SIGNAL(rowsRemoved(const QModelIndex&, int, int)), this, SLOT(check_emptiness()));
        connect(model(), SIGNAL(modelReset()), this, SLOT(check_emptiness()));
    }

    void FileItemList::add_entries(const QStringList& entries)
    {
        FileItemCreator creator(entries, thread(), settings_manager()->get_settings()->AddRecursively());
        QProgressDialog progress(this);
        QTimer timer;

        progress.setWindowTitle(tr("Adding files..."));
        progress.setMaximum(0);
        timer.setSingleShot(true);

        connect(&creator, SIGNAL(about_to_finish()), &timer, SLOT(stop()));
        connect(&creator, SIGNAL(finished()), &progress, SLOT(accept()));
        connect(&creator, SIGNAL(progress_changed(const QString&)), &progress, SLOT(setLabelText(const QString&)));
        connect(&creator, SIGNAL(completed(const QList<QTreeWidgetItem*>&)), this, SLOT(add_items(const QList<QTreeWidgetItem*>&)));
        connect(&progress, SIGNAL(canceled()), &creator, SLOT(cancel()));
        connect(&timer, SIGNAL(timeout()), &progress, SLOT(open()));

        timer.start(1000);
        creator.start();

        while (creator.isRunning()) {
            qApp->processEvents();
        }
    }

    bool FileItemList::has_running_items(bool from_selected)
    {
        const QList<FileItem*> items = qitems_cast(from_selected ? selectedItems() : all_items(this));
        return items.constEnd() != std::find_if(items.constBegin(), items.constEnd(), [](const FileItem* item){return item->is_running();});
    }

    size_t FileItemList::count_running_items(bool from_selected)
    {
        const QList<FileItem*> items = qitems_cast(from_selected ? selectedItems() : all_items(this));
        return std::count_if(items.constBegin(), items.constEnd(), [](FileItem* item){return item->is_running();});
    }

    void FileItemList::translate()
    {
        const QList<FileItem*> existing_items = qitems_cast(all_items(this));
        std::for_each(existing_items.constBegin(), existing_items.constEnd(), [](FileItem* item){item->translate();});
    }

    void FileItemList::select_all_items()
    {
        selectAll();
    }

    void FileItemList::select_none_items()
    {
        clearSelection();
    }

    void FileItemList::remove_selected_items()
    {
        if (has_running_items(true) && QMessageBox::No == QMessageBox::question(this, Info::get_app_name(), tr("There are unfinished conversions.<br>Are you sure you want to remove selected files from the list?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No)) {
            return;
        }
        qDeleteAll(selectedItems());
    }

    void FileItemList::remove_all_items()
    {
        if (has_running_items() && QMessageBox::No == QMessageBox::question(this, Info::get_app_name(), tr("There are unfinished conversions.<br>Are you sure you want to remove all files from the list?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No)) {
            return;
        }
        clear();
    }

    void FileItemList::start_selected_items()
    {
        QList<FileItem*> selected_items = qitems_cast(selectedItems());
        QList<FileItem*> ready_to_start_items;

        std::copy_if(selected_items.constBegin(), selected_items.constEnd(), std::back_inserter(ready_to_start_items), [](FileItem* item){return !item->is_running();});

        QList<FileItem*> items_to_start = ready_to_start_items.mid(0, settings_manager()->get_settings()->SimultaneousConversions() - count_running_items());
        QList<FileItem*> items_to_queue = ready_to_start_items.mid(items_to_start.count());

        std::for_each(items_to_queue.constBegin(), items_to_queue.constEnd(), [](FileItem* item){item->queue();});
        std::for_each(items_to_start.constBegin(), items_to_start.constEnd(), [](FileItem* item){item->start();});
    }

    void FileItemList::stop_selected_items()
    {
        QList<FileItem*> items = qitems_cast(selectedItems());
        std::for_each(items.constBegin(), items.constEnd(), [](FileItem* item){item->stop();});
    }

    void FileItemList::dragEnterEvent(QDragEnterEvent* event)
    {
        if (event->mimeData()->hasUrls()) {
            event->acceptProposedAction();
        }
    }

    void FileItemList::dragMoveEvent(QDragMoveEvent* event)
    {
        if (event->mimeData()->hasUrls()) {
            event->acceptProposedAction();
        }
    }

    void FileItemList::dropEvent(QDropEvent* event)
    {
        QList<QUrl> urls = event->mimeData()->urls();
        QStringList entries;
        std::transform(urls.constBegin(), urls.constEnd(), std::back_inserter(entries), std::mem_fun_ref(&QUrl::toLocalFile));
        add_entries(entries);
    }

    void FileItemList::add_items(const QList<QTreeWidgetItem*>& new_qitems)
    {
        if (new_qitems.size() > 1000 && QMessageBox::No == QMessageBox::warning(this, tr("Warning"), tr("Are you sure you want to add %1 files?<br>This may decrease performance considerably.").arg(new_qitems.size()), QMessageBox::Yes | QMessageBox::No, QMessageBox::No)) {
            qDeleteAll(new_qitems);
            return;
        }

        QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

        QList<FileItem*> new_items = qitems_cast(new_qitems);
        QList<FileItem*> existing_items = qitems_cast(all_items(this));
        QList<FileItem*> unique_items;

        std::remove_copy_if(new_items.constBegin(), new_items.constEnd(), std::back_inserter(unique_items), std::bind2nd(FileItemExists(), existing_items));
        std::for_each(unique_items.begin(), unique_items.end(), [this](FileItem* item){connect(item, SIGNAL(task_finished()), this, SLOT(do_task_finished()));});

        addTopLevelItems(items_cast(unique_items));

        if (settings_manager()->get_settings()->AddToSelection()) {
            std::for_each(unique_items.begin(), unique_items.end(), [](FileItem* item){item->setSelected(true);});
        }

        QApplication::restoreOverrideCursor();
    }

    void FileItemList::check_selection()
    {
        Q_EMIT selected(!selectedItems().empty());
    }

    void FileItemList::check_emptiness()
    {
        Q_EMIT empty(0 == topLevelItemCount());
    }

    void FileItemList::do_item_double_clicked(QTreeWidgetItem* item, int)
    {
        if (0 != item) {
            qitem_cast(item)->show_info_dialog();
        }
    }

    void FileItemList::do_task_finished()
    {
        QList<FileItem*> existing_items = qitems_cast(all_items(this));

        QList<FileItem*> items_to_start;
        std::copy_if(existing_items.constBegin(), existing_items.constEnd(), std::back_inserter(items_to_start), [](FileItem* item){return item->is_queued();});

        items_to_start = items_to_start.mid(0, settings_manager()->get_settings()->SimultaneousConversions() - count_running_items());
        std::for_each(items_to_start.constBegin(), items_to_start.constEnd(), [](FileItem* item){item->start();});
    }
}
