﻿/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_FILE_ITEM_LIST_HPP
#define MEDIA_CONVERTER_HEADER_FILE_ITEM_LIST_HPP

#include <QStringList>
#include <QTreeWidget>

namespace MediaConverter
{
    class FileItemList: public QTreeWidget
    {
        Q_OBJECT

    public:
        explicit FileItemList(QWidget* parent = nullptr);

        void add_entries(const QStringList& entries);

        bool has_running_items(bool from_selected = false);
        size_t count_running_items(bool from_selected = false);

    public Q_SLOTS:
        void translate();

        void select_all_items();
        void select_none_items();

        void remove_selected_items();
        void remove_all_items();

        void start_selected_items();
        void stop_selected_items();

    Q_SIGNALS:
        void selected(bool);
        void empty(bool);

    protected:
        virtual void dragEnterEvent(QDragEnterEvent* event);
        virtual void dragMoveEvent(QDragMoveEvent* event);
        virtual void dropEvent(QDropEvent* event);

    private Q_SLOTS:
        void add_items(const QList<QTreeWidgetItem*>& items);

        void check_selection();
        void check_emptiness();

        void do_item_double_clicked(QTreeWidgetItem* item, int column);
        void do_task_finished();
    };
}

#endif
