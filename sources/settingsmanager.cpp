/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "settingsmanager.hpp"
#include "info.hpp"
#include "logging.hpp"
#include "service.hpp"

#include <thread>

namespace MediaConverter
{
    SettingsManager::SettingsManager():
        Singleton<SettingsManager>(this),
        qt_settings(MediaConverter::Info::get_author_internal_name(), MediaConverter::Info::get_app_internal_name()),
        app_settings(new Settings)
    {
        if (!qt_settings.isWritable()) {
            service()->warning(QSettings::tr("Settings are not writable"));
        }

        switch (qt_settings.status()) {
        case QSettings::NoError:
            break;
        case QSettings::AccessError:
            service()->error(QSettings::tr("Settings") + ": " + QSettings::tr("an access error occurred"));
            break;
        case QSettings::FormatError:
            service()->error(QSettings::tr("Settings") + ": " + QSettings::tr("a format error occurred"));
            break;
        default:
            Q_ASSERT_X(false, Q_FUNC_INFO, "Unsupported version of the Qt library");
            service()->fatal(QSettings::tr("Unsupported version of the Qt library"));
        }

        app_settings->Load(qt_settings);

        switch (app_settings->Version())
        {
        case 1:
            break;
        default:
            service()->fatal(QSettings::tr("Unsupported version of the program options"));
        }

        if (app_settings->FirstStart()) {
            app_settings->SetSimultaneousConversions(autodetect_simultaneous_conversions());
            app_settings->SetFirstStart(false);
        }

        log(QSettings::tr("Language directory is '%1'").arg(app_settings->LanguagePath()));
    }

    SettingsManager::~SettingsManager()
    {
        app_settings->Save(qt_settings);
    }

    const SettingsPtr SettingsManager::clone_settings() const
    {
        return SettingsPtr::create(*app_settings);
    }

    size_t SettingsManager::autodetect_simultaneous_conversions()
    {
        return std::max<size_t>(1, std::thread::hardware_concurrency());
    }
}
