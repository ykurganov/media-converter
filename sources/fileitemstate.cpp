﻿/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "fileitemstate.hpp"
#include "fileitem.hpp"

namespace MediaConverter
{
    // --------------------------------------------------READY--------------------------------------------------

    FileItemStateReady::FileItemStateReady(FileItem* file_item)
    {
        file_item->setToolTip(FileItem::Column_Status, "");
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressValue, 0);
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressStyleSheet,
            "QProgressBar { border: 2px solid grey; border-radius: 5px; color: black; }"
            "QProgressBar::chunk { background-color: white; }"
        );
        translate(file_item);
    }

    void FileItemStateReady::translate(FileItem* file_item) const
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressFormat, FileItem::tr("Ready"));
    }

    // --------------------------------------------------QUEUED--------------------------------------------------

    FileItemStateQueued::FileItemStateQueued(FileItem* file_item)
    {
        file_item->setToolTip(FileItem::Column_Status, "");
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressValue, 0);
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressStyleSheet,
            "QProgressBar { border: 2px solid grey; border-radius: 5px; color: black; }"
            "QProgressBar::chunk { background-color: white; }"
        );
        translate(file_item);
    }

    void FileItemStateQueued::translate(FileItem* file_item) const
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressFormat, FileItem::tr("Queued"));
    }

    // --------------------------------------------------RUNNING--------------------------------------------------

    FileItemStateRunning::FileItemStateRunning(FileItem* file_item)
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressValue, 0);
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressFormat, "%p%");
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressStyleSheet,
            "QProgressBar { border: 2px solid grey; border-radius: 5px; color: black; }"
            "QProgressBar::chunk { background-color: blue; }"
        );
        translate(file_item);
    }

    void FileItemStateRunning::translate(FileItem*) const
    {
    }

    // --------------------------------------------------FINISHED--------------------------------------------------

    FileItemStateFinished::FileItemStateFinished(FileItem* file_item)
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressValue, 100);
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressStyleSheet,
            "QProgressBar { border: 2px solid grey; border-radius: 5px; color: white; }"
            "QProgressBar::chunk { background-color: blue; }"
        );
        translate(file_item);
    }

    void FileItemStateFinished::translate(FileItem* file_item) const
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressFormat, FileItem::tr("Finished"));
    }

    // --------------------------------------------------STOPPED--------------------------------------------------

    FileItemStateStopped::FileItemStateStopped(FileItem* file_item)
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressValue, 100);
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressStyleSheet,
            "QProgressBar { border: 2px solid grey; border-radius: 5px; color: white; }"
            "QProgressBar::chunk { background-color: red; }"
        );
        translate(file_item);
    }

    void FileItemStateStopped::translate(FileItem* file_item) const
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressFormat, FileItem::tr("Stopped"));
    }

    // --------------------------------------------------COMPLETED--------------------------------------------------

    FileItemStateCompleted::FileItemStateCompleted(FileItem* file_item)
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressValue, 100);
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressStyleSheet,
            "QProgressBar { border: 2px solid grey; border-radius: 5px; color: white; }"
            "QProgressBar::chunk { background-color: green; }"
        );
        translate(file_item);
    }

    void FileItemStateCompleted::translate(FileItem* file_item) const
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressFormat, FileItem::tr("Completed"));
    }

    // --------------------------------------------------ERROR--------------------------------------------------

    FileItemStateError::FileItemStateError(FileItem* file_item)
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressValue, 100);
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressStyleSheet,
            "QProgressBar { border: 2px solid grey; border-radius: 5px; color: white; }"
            "QProgressBar::chunk { background-color: red; }"
        );
        translate(file_item);
    }

    void FileItemStateError::translate(FileItem* file_item) const
    {
        file_item->setData(FileItem::Column_Status, FileItem::Role_ProgressFormat, FileItem::tr("Error"));
    }
}
