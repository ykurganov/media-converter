# -*- coding: utf-8 -*-

# This file is part of Media Converter.
#
# Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
Import("env")

env = env.Clone()

DEBNAME = "media-converter"
DEBVERSION = "0.5"
DEBMAINT = "Yanis Kurganov <yanis.kurganov@gmail.com>"
DEBARCH = "amd64"
DEBDEPENDS = "libarchive13, libqt5core5a, libqt5gui5, libqt5widgets5"
DEBDESC = "Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools"

DEBFILES = [
    # Now we specify the files to be included in the .deb
    # Where they should go, and where they should be copied from.
    # If you have a lot of files, you may wish to generate this
    # list in some other way.
    ("usr/bin/media-converter",             "#build/binary/linux-gnu-x64/debug/media-converter"),
]

# This is the debian package we're going to create
debpkg = '#%s-%s-%s.deb' % (DEBNAME, DEBVERSION, DEBARCH)

DEBCONTROLFILE = os.path.join("#build", "debian", "control")

# This copies the necessary files into place into place.
# Fortunately, SCons creates the necessary directories for us.
for f in DEBFILES:
    # We put things in a directory named after the package
    dest = os.path.join("#build", "debian", f[0])
    # The .deb package will depend on this file
    env.Depends(debpkg, dest)
    # Copy from the the source tree.
    env.Command(dest, f[1], Copy('$TARGET', '$SOURCE'))
    # The control file also depends on each source because we'd like
    # to know the total installed size of the package
    env.Depends(DEBCONTROLFILE, dest)

# Now to create the control file:

CONTROL_TEMPLATE = """
Package: %s
Priority: optional
Section: utils
Installed-Size: %s
Maintainer: %s
Architecture: %s
Version: %s
Depends: %s
Description: %s

"""

env.Depends(debpkg, DEBCONTROLFILE)

# This function creates the control file from the template and info
# specified above, and works out the final size of the package.
def make_control(target=None, source=None, env=None):
    installed_size = 0
    for i in DEBFILES:
        installed_size += os.stat(str(env.File(i[1])))[6]
    control_info = CONTROL_TEMPLATE % (DEBNAME, installed_size, DEBMAINT, DEBARCH, DEBVERSION, DEBDEPENDS, DEBDESC)
    f = open(str(target[0]), 'w')
    f.write(control_info)
    f.close()

# We can generate the control file by calling make_control
env.Command(DEBCONTROLFILE, None, make_control)

# And we can generate the .deb file by calling dpkg-deb
env.Command(debpkg, DEBCONTROLFILE, "fakeroot dpkg-deb -b %s %s" % ("build/deploy", "$TARGET"))

targets = [debpkg]

Return("targets")
