# -*- coding: utf-8 -*-

# This file is part of Media Converter.
#
# Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
# Copyright (C) 2007-2016 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os, SCons
Import("env")

env = env.Clone()

targets = [
    env.Command(
        os.path.join("$SCONSX_BUILD_PATH", "languages", os.path.basename(os.path.dirname(node.path)), node.name),
        node,
        Copy("$TARGET", "$SOURCE")
    ) for parentNode in env.Glob("*") if isinstance(parentNode, SCons.Node.FS.Dir) for node in parentNode.glob("*") if isinstance(node, SCons.Node.FS.File) and node.suffix != ".ts"
]

Return("targets")
